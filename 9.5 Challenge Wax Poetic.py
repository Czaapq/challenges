import random



nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]

verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]

adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]

prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]

adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

articles_list = ("a", "e", "i", "o", "u") #wrzucamy w tuple a nie w liste dla startwith

noun1 = random.choice(nouns)
noun2 = random.choice(nouns)
noun3 = random.choice(nouns)
while noun2 == noun1:
    noun2 = random.choice(nouns)
while noun3 == noun2 or noun3 == noun1: 
    noun3 = random.choice(nouns)
#petla dla nouns przypisuje 3 rozne slowa


verb1 = random.choice(verbs)
verb2 = random.choice(verbs)
verb3 = random.choice(verbs)
while verb2 == verb1:
    verb2 = random.choice(verbs)
while verb3 == verb2 or verb3 == verb1:
    verb3 = random.choice(verbs)
#petka dla verbs przypisuje 3 rozne slowa


adjective1 = random.choice(adjectives)
adjective2 = random.choice(adjectives)
adjective3 = random.choice(adjectives)
while adjective2 == adjective1:
    adjective2 = random.choice(adjectives)
while adjective3 == adjective2 or adjective3 == adjective1:
    adjective3 = random.choice(adjectives)
#petla dla adjectives przypisuje 3 rozne slowa


preposition1 = random.choice(prepositions)
preposition2 = random.choice(prepositions)
while preposition2 == preposition1:
    preposition2 = random.choice(prepositions)
#petla dla prepositions przypisuje 2 rozne slowa


adverb = random.choice(adverbs)

if adjective1.startswith(articles_list): #arg w startwith musi być: str or a tuple of str. 
    articles = "An"                      #startwith sprawdza na jaka litere zaczyna sie adjective1
else:
    articles = "A"



print(f"{articles} {adjective1} {noun1}")
print("\n")
print(f"{articles} {adjective1} {noun1} {verb1} {preposition1} the {adjective2} {noun2}")
print(f"{adverb}, the {noun1} {verb2}")
print(f"the {noun2} {verb3} {preposition2} a {adjective3} {noun3}")
