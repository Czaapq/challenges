import csv
from pathlib import Path


best_Score = {}

scores_path = Path.cwd() / "12.7 Challenge High Scores" / "scores.csv" 

with scores_path.open(mode="r", encoding="utf-8")as csv_file:
    reader = csv.DictReader(csv_file)
    all_scores = [row for row in reader]


for record in all_scores:
    name = record['name']
    score = int(record['score'])
    #print(name, score)
    if name not in best_Score:
        best_Score[name] = score
    else:
        if score > best_Score[name]:
            best_Score[name] = score

print(best_Score)



high_scores_path = Path.cwd() / "12.7 Challenge High Scores" / "high_scores.csv"

with high_scores_path.open(mode="w", encoding="utf-8", newline="") as csv_best_sc_file:
    writer = csv.DictWriter(csv_best_sc_file, fieldnames=['name', 'best_score'])
    writer.writeheader()
    for name in best_Score:
        row = {'name': name, 'best_score': best_Score[name]}
        writer.writerow(row)