class Animal:
    sleep_time = 3
    eat_amount = 0

    def __init__(self, name, age, sound):
        self.name = name
        self.age = age
        self.sound = sound

    def introduce(self):
        return (f"I am My name is {self.name}, and I'm {self.age} years old ")

    def sleep(self):
        self.sleep_time
        for _ in range(self.sleep_time):
            print(f"ZzZzZZzz")
        return (f"{self.name} finished sleeping")

    def eat(self):
        self.eat_amount = self.eat_amount + 1
        if self.eat_amount > 4:
            return (f"{self.name} is not hungry anymore")
        else:
            return (f"{self.name} is hungry!!")

    def make_sound(self):
        return (f"{self.name} says {self.sound}")


class Cow(Animal):

    def __init__(self, name, age, sound, amount_of_milk):
        super().__init__(name, age, sound)
        self.amount_of_milk = amount_of_milk

    def milking_cow(self):
        if self.amount_of_milk <= 0:
            return (f"you can't get milk form {self.name}")
        else:
            return (f"you got {self.amount_of_milk} buckets of milk form {self.name}")


class Pig(Animal):

    def mud_bath(self):
        return (f"{self.name} takes a mud bath")


class Dog(Animal):

    def run(self, running_speed):
        if running_speed == 0:
            return (f"{self.name} doesn't run")
        elif running_speed > 10:
            return (f"{self.name} run very fast")
        elif running_speed < 10:
            return (f"{self.name} run pretty slow")


barbara = Cow("Barbara", 8, "Moooo", 4)
jana = Cow("jana", 3, "Moooo", 0)


pepa = Pig("Pepa", 4, "oink oink")

zeus = Dog("zeus", 9, "woof, woof")
milo = Dog("milo", 8, "grrWOOF, grrWOOF")


print(zeus.run(12))
print(milo.run(5))

for i in range(5):
    print(pepa.eat())
print(pepa.sleep())


print(barbara.milking_cow())
print(jana.make_sound())
print(jana.milking_cow())
