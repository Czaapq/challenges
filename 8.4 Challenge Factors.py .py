

user_number = int(input("Enter a positive integer: "))


for number in range(1, user_number+1):
    if (user_number % number == 0):
        print(f"{number} is a factor of {user_number}")
