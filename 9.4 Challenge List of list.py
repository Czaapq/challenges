


def enrollment_stats(list_of_list):
    students_values = []   
    tuition_values = []
    
    for value in list_of_list:
        students_values.append(value[1]) #można za pomoca list comp.
        tuition_values.append(value[2])
    
    return students_values,tuition_values




def mean(to_mean):
    avg_value = 0
    for value in to_mean: 
        avg_value = avg_value + value   #można użyć sum(), byłoby ladniej
    return (avg_value / len(to_mean))





def median(to_median):
    param_length = len(to_median)
    value_sorted = sorted(to_median)
    if param_length % 2 == 0:
        first_numb = value_sorted[param_length // 2]        #tutaj w [] definiujemy index pierwszej liczby i przypisujemy do first_numb
        second_numb = value_sorted[param_length // 2 -1]    #również w [] definiujemy index i przypisujemy do second_numb
        result = (first_numb + second_numb) / 2             #mediana parzystych == dwie srodkowe + do siebie potem / na dwa
        return result
    elif param_length % 2 == 1:
        middle_numb = value_sorted[param_length // 2]       #mediana jezeli jest nieparzyscie 
        result = middle_numb
        return result
    




universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]


students, tuition = enrollment_stats(universities)


print("******************************")
print(f"Total students:     {sum(students):,}")
print(f"Total tuition:    $ {sum(tuition):,}")
print("\n")
print(f"Student mean:       {mean(students):,.2f}")
print(f"Student median:     {median(students):,}")
print("\n")
print(f"Tuition mean:     $ {mean(tuition):,.2f}")
print(f"Tuition median:   $ {median(tuition):,}")
print("******************************")