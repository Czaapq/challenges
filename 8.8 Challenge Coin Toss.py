

import random


def coin_flipper():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


how_many_attempt = 10_000
how_many_flips = 0


for i in range(how_many_attempt):
    if coin_flipper() == "heads":
        how_many_flips = how_many_flips + 1
        while coin_flipper() == "heads":  # w pętli rzuca moneta i dodaje do flips jak jest heads
            how_many_flips = how_many_flips + 1
        how_many_flips = how_many_flips + 1
        # tutaj dodaje jak wyjdzie z while zeby zaliczyc rzut = tails

    else:
        how_many_flips = how_many_flips + 1
        while coin_flipper() == "tails":
            how_many_flips = how_many_flips + 1
        how_many_flips = how_many_flips + 1


average_number = (how_many_flips/how_many_attempt)

print(f"average number of flips per trial: {average_number:.2f}")
