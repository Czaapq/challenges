

def invest(amount, rate, years):
    rate = (rate / 100)
    for year in range(1, years+1):
        amount = amount + amount * rate
        print(f"year {year}: ${amount:,.2f}")


initial_amount = float(input("Enter an initial amount: $"))
percentage_rate = float(input("Enter percentage rate: %"))
years_number = int(input("Enter number of years: "))


invest(initial_amount, percentage_rate, years_number)
