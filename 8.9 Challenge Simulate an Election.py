
import random


def who_won(chance):
    if random.random() < chance:
        return 1
    else:
        return 0


region_1_A_chance = 0.87
region_2_A_chance = 0.65
region_3_A_chance = 0.17


candidate_A_won = 0
election_amount = 10_000


for i in range(election_amount + 1):
    reg_1_result = who_won(region_1_A_chance)
    reg_2_result = who_won(region_2_A_chance)
    reg_3_result = who_won(region_3_A_chance)
    if (reg_1_result + reg_2_result + reg_3_result) >= 2:
        candidate_A_won = candidate_A_won + 1


times_A_won = candidate_A_won / election_amount

print(f"Percentage of times in which Candidate A wins: {times_A_won:.2%} ")
