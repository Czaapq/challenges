

def convert_far_to_cel(far_temperature):
    cel_temp = ((far_temperature - 32) * 5/9)
    return cel_temp


def convert_cel_to_far(cel_temperature):
    far_temp = (cel_temperature * 9/5 + 32)
    return far_temp


user_far_temp = float(input("Enter a temperature in degrees F: "))

print(f"{user_far_temp} degrees F = {convert_far_to_cel(user_far_temp):.2f} degrees C")


user_cel_temp = float(input("Enter a temperature in degrees C: "))

print(f"{user_cel_temp} degrees C = {convert_cel_to_far(user_cel_temp):.2f} degrees F")
