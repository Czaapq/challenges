from pathlib import Path


documents_folder = Path.cwd()/ "12.4 Challenge Move All Image" / "Practice Files" / "Documents Folder"

new_images_folder = Path.cwd()/ "12.4 Challenge Move All Image" / "New Images Folder"
new_images_folder.mkdir()



for plik in documents_folder.rglob("*.*"):
    if plik.suffix in [".png", ".jpg", ".gif"]:
        plik.replace(new_images_folder / plik.name)