import random



capitals_dict = {
'Alabama': 'Montgomery',
'Alaska': 'Juneau',
'Arizona': 'Phoenix',
'Arkansas': 'Little Rock',
'California': 'Sacramento',
'Colorado': 'Denver',
'Connecticut': 'Hartford',
'Delaware': 'Dover',
'Florida': 'Tallahassee',
'Georgia': 'Atlanta',
}



states_and_capitals = random.choice(list(capitals_dict.items()))

state = states_and_capitals[0] #przypisujemy sobie state
capital = states_and_capitals[1] #przypisujemy sobie capital


print("***************************************")
print("Hi, let's play a game.")
print("I will show you the name of state and")
print("you must guess the capital")
print("if you want exit, just type: exit")
print("***************************************")
print(f"what is the capital of state: {state}?")
print("***************************************")



while True:
    answer = input("Your answer:   ").lower() #pamietać ze za lower musi byc ()!
   
    if answer == capital.lower(): #sprawdza czy pasuje, break konczy petle
        print("WOW! GOOD JOB!")
        break
    
    elif answer == "exit": #pozwala wyjsc jesli input == "exit", break konczy petle
        print(f"Capital of {state} is {capital}. Goodbye, see you next time :)")
        break

    elif answer != capital.lower(): #jeśli nie zgadniemy, robi printa
        print("WRONG! TRY AGAIN")

    